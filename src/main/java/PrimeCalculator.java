import java.util.ArrayList;
import java.util.List;

class PrimeCalculator {
    static int MAX_SIZE = 1000005;
    private int nth;

    int nth(int nth) {
        this.nth = nth;
        List<Integer> primes = getPrimes();
        return primes.get(nth - 1);

    }

    private List<Integer> getPrimes() {
        List<Integer> primes = new ArrayList<>();
        if (nth < 1) {
            throw new PositiveNumberOnly();
        } else {

            int[] allNumber = new int[MAX_SIZE];
            for (int i = 0; i < MAX_SIZE; i++) {
                allNumber[i] = i + 1;
            }

            for (int j : allNumber) {
                int currentPrime = j;
                if (currentPrime == 1 || currentPrime == -1) continue;
                primes.add(currentPrime);
                for (int master = currentPrime; currentPrime < Math.sqrt(MAX_SIZE) && master <= MAX_SIZE; master = master + currentPrime) {
                    allNumber[master - 1] = -1;
                }
            }
            return primes;

        }
    }
}
